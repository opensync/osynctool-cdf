# msyntool(1) completion 
# put this file in /etc/bash_completion.d/ 
# Copyright: (C) Copyright 2008 by Michael Bell <michael.bell@cms.hu-berlin.de>
# License:   GNU GPL v2 or later


__osynctool_is_name_in_list()
{
	local TRUE=0
	local FALSE=1
	local NAME=$1
	local LIST=$2
	if [[ "$NAME" -eq "" ]]; then
		return $FALSE;
	fi
	for ITEM in $LIST; do
		if [[ "${NAME}" == "${ITEM}" ]]; then
			return $TRUE
		fi
	done
	return $FALSE
}

__osynctool_get_groups()
{
	local OSYNCTOOL=$1
	OSYNCTOOL_GROUPLIST=`${OSYNCTOOL} --listgroups | sed 's%.*:%%'`;
}

__osynctool_get_objtypes()
{
	local OSYNCTOOL=$1
	local GROUP=$2
	OSYNCTOOL_OBJTYPELIST=`${OSYNCTOOL} --showobjtypes ${GROUP} | grep -i "\(enabled\|disabled\)" | grep -v -i objtype | sed 's%:.*%%'`
}

__osynctool_get_members()
{
	local OSYNCTOOL=$1
	local GROUP=$2
	OSYNCTOOL_MEMBERLIST=`${OSYNCTOOL} --showgroup ${GROUP} | grep -i "^member" | sed 's%Member %%i' | sed 's%:.*%%'`;
}

## have osynctool &&
_osynctool()
{
	# put all the different commands into variables
	# every variable contains all params with the same arguments

	local SINGLE="\
		--listplugins \
		--listformats \
		--version \
		--help \
		--listgroups \
		--wait \
		--multi \
		--manual"

	local GROUP="\
		--showgroup \
		--showobjtypes \
		--showfilter \
		--showcapabilities \
		--addgroup \
		--delgroup \
		--configure-filter \
		--enable-merger \
		--disable-merger \
		--enable-converter \
		--disable-converter \
		--sync"

	local OBJTYPE="\
		--enable-objtype \
		--disable-objtype"

	local CONFLICT="\
		--conflict"

	local ADDMEMBER="\
		--addmember"

	local MEMBER="\
		--delmember \
		--configure \
		--configure-capabilities"

	local DISCOVER="\
		--discover"

	local READONLY="\
		--disable-readonly \
		--enable-readonly"

	local SLOWSYNC="\
		--slow-sync"

	local CONFIGDIR="\
		--configdir"

	local OPTIONS="\
			"

	# prepare some special variables

	local PARAMS="${SINGLE} ${GROUP} ${OBJTYPE} ${CONFLICT} ${ADDMEMBER} ${MEMBER} ${DISCOVER} ${READONLY} ${SLOWSYNC} ${CONFIGDIR}"

        local current=${COMP_WORDS[COMP_CWORD]}
    	local all="${COMP_LINE}"

	local current_minus1=""
	if [ $COMP_CWORD -gt 1 ]; then
		current_minus1=${COMP_WORDS[${COMP_CWORD}-1]}
	fi
	local current_minus2=""
	if [ $COMP_CWORD -gt 2 ]; then
		current_minus2=${COMP_WORDS[${COMP_CWORD}-2]}
	fi
	local current_minus3=""
	if [ $COMP_CWORD -gt 3 ]; then
		current_minus3=${COMP_WORDS[${COMP_CWORD}-3]}
	fi

	# try to detect the correct proposals and
	# fill the COMPREPLY accordingly
	# the order is important

        COMPREPLY=()

	# scan for missing third argument

	if __osynctool_is_name_in_list "$current_minus3" "${READONLY}"; then
		__osynctool_get_objtypes $1 ${current_minus2}
                COMPREPLY=( $( compgen -W '${OSYNCTOOL_OBJTYPELIST} ${PARAMS}' -- $current ) )

	# scan for missing second argument

	elif __osynctool_is_name_in_list "$current_minus2" "${OBJTYPE}"; then
		__osynctool_get_objtypes $1 ${current_minus1}
                COMPREPLY=( $( compgen -W '${OSYNCTOOL_OBJTYPELIST}' -- $current ) )
	elif __osynctool_is_name_in_list "$current_minus2" "${ADDMEMBER}"; then
		local PLUGINLIST=`$1 --listplugins | sed 's%.*:%%'`;
                COMPREPLY=( $( compgen -W '${PLUGINLIST}' -- $current ) )
	elif __osynctool_is_name_in_list "$current_minus2" "${MEMBER} ${READONLY}"; then
		__osynctool_get_members $1 ${current_minus1}
                COMPREPLY=( $( compgen -W '${OSYNCTOOL_MEMBERLIST}' -- $current ) )
	elif __osynctool_is_name_in_list "$current_minus2" "${DISCOVER}"; then
		__osynctool_get_members $1 ${current_minus1}
                COMPREPLY=( $( compgen -W '${OSYNCTOOL_MEMBERLIST} ${PARAMS}' -- $current ) )

	# scan for missing first argument

	elif __osynctool_is_name_in_list "$current_minus1" "${GROUP} ${OBJTYPE} ${ADDMEMBER} ${MEMBER} ${DISCOVER} ${READONLY}"; then
		__osynctool_get_groups $1
                COMPREPLY=( $( compgen -W '${OSYNCTOOL_GROUPLIST}' -- $current ) )
	elif __osynctool_is_name_in_list "$current_minus1" "${CONFLICT}"; then
		# there is a problem here
		# it is not clear which group is used and
		# so we can only display all known memberids
		__osynctool_get_groups $1
		local MEMBERLIST="";
		for ITEM in ${OPENSYNC_GROUPLIST}; do
			__osynctool_get_members $1 ${ITEM}
			MEMBERLIST="${MEMBERLIST} ${OSYNCTOOL_MEMBERLIST}"
		done
		MEMBERLIST=`echo ${MEMBERLIST} | sort | uniq`
                COMPREPLY=( $( compgen -W '${MEMBERLIST} d i n' -- $current ) )
	elif __osynctool_is_name_in_list "$current_minus1" "${SLOWSYNC}"; then
		# there is a problem here
		# it is no clear which group is used and
		# so we can only display all known objtypes
		__osynctool_get_groups $1
		local OBJTYPELIST="";
		for ITEM in ${OPENSYNC_GROUPLIST}; do
			__osynctool_get_objtypes $1 ${ITEM}
			OBJTYPELIST="${OBJTYPELIST} ${OSYNCTOOL_OBJTYPELIST}"
		done
		OBJTYPELIST=`echo ${OBJTYPELIST} | sort | uniq`
                COMPREPLY=( $( compgen -W '${OBJTYPELIST}' -- $current ) )
	elif __osynctool_is_name_in_list "$current_minus1" "${CONFIGDIR}"; then
		# _filedir
		_cd

	# show all available commands

	else
                COMPREPLY=( $( compgen -W '${PARAMS}' -- $current ) ) 
        fi

}
## [ "${have:-}" ] && complete -F _osynctool osynctool
complete -F _osynctool osynctool
