/*
 * osynctool - A command line client for the opensync framework
 * Copyright (C) 2013 Mark Ellis <mark@mpellis.org.uk> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 */
 
#include <opensync/opensync.h>
#include <opensync/opensync-plugin.h>
#include <opensync/opensync-format.h>

#include "osynctool_print_config.h"

static void
osynctool_print_config_authentication(OSyncPluginAuthentication *auth)
{
	const char *username, *password, *ref;
	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, auth);

	printf("\tAuthentication\n");

	if ((username = osync_plugin_authentication_get_username(auth)))
		printf("\t\tUsername: %s\n", username);

	if ((password = osync_plugin_authentication_get_password(auth)))
		printf("\t\tPassword: %s\n", password);

	if ((ref = osync_plugin_authentication_get_reference(auth)))
		printf("\t\tReference: %s\n", ref);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_externalplugin(OSyncPluginExternalPlugin *externalplugin)
{
	const char *external_command;
	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, externalplugin);

	printf("\tExternalPlugin\n");

	if ((external_command = osync_plugin_externalplugin_get_external_command(externalplugin)))
		printf("\tExternalCommand: %s\n", external_command);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_connection(OSyncPluginConnection *conn)
{
	const char *mac, *sdpuuid, *address, *protocol, *dnssd, *devicenode, *service, *vendorid, *productid;
	unsigned int rfcomm_channel, interf, port, speed;
	OSyncPluginConnectionType conn_type;
	const char *conn_str = NULL;

	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, conn);

	printf("\tConnection\n");

	/* Active Connection */
	conn_type = osync_plugin_connection_get_type(conn);

	switch (conn_type) {
	case OSYNC_PLUGIN_CONNECTION_BLUETOOTH:
		conn_str = "Bluetooth";
		break;
	case OSYNC_PLUGIN_CONNECTION_USB:
		conn_str = "USB";
		break;
	case OSYNC_PLUGIN_CONNECTION_NETWORK:
		conn_str = "Network";
		break;
	case OSYNC_PLUGIN_CONNECTION_SERIAL:
		conn_str = "Serial";
		break;
	case OSYNC_PLUGIN_CONNECTION_IRDA:
		conn_str = "IrDA";
		break;
	default:
		conn_str = "Unknown";
		break;
	}
	printf("\t\tActiveConnection: %s\n", conn_str);

	/* Bluetooth */
	if (osync_plugin_connection_is_supported(conn, OSYNC_PLUGIN_CONNECTION_BLUETOOTH)) {
		printf("\t\tBluetooth\n");

		if ((mac = osync_plugin_connection_bt_get_addr(conn)))
			printf("\t\t\tMAC: %s\n", mac);

		if ((rfcomm_channel = osync_plugin_connection_bt_get_channel(conn)))
			printf("\t\t\tRFCommChannel: %u\n", rfcomm_channel);

		if ((sdpuuid = osync_plugin_connection_bt_get_sdpuuid(conn)))
			printf("\t\t\tSDPUUID: %s\n", sdpuuid);
	}

	/* USB */
	if (osync_plugin_connection_is_supported(conn, OSYNC_PLUGIN_CONNECTION_USB)) {
		printf("\t\tUSB\n");

		if ((vendorid = osync_plugin_connection_usb_get_vendorid(conn)))
			printf("\t\t\tVendorID: %s\n", vendorid);

		if ((productid = osync_plugin_connection_usb_get_productid(conn)))
			printf("\t\t\tProductID: %s\n", productid);

		if ((interf = osync_plugin_connection_usb_get_interface(conn)))
			printf("\t\t\tInterface: %u\n", interf);
	}

	/* Network */
	if (osync_plugin_connection_is_supported(conn, OSYNC_PLUGIN_CONNECTION_NETWORK)) {
		printf("\t\tNetwork\n");

		if ((address = osync_plugin_connection_net_get_address(conn)))
			printf("\t\t\tAddress: %s\n", address);

		if ((port = osync_plugin_connection_net_get_port(conn)))
			printf("\t\t\tPort: %u\n", port);

		if ((protocol = osync_plugin_connection_net_get_protocol(conn)))
			printf("\t\t\tProtocol: %s\n", protocol);

		if ((dnssd = osync_plugin_connection_net_get_dnssd(conn)))
			printf("\t\t\tDNSSD: %s\n", dnssd);
	}

	/* Serial */
	if (osync_plugin_connection_is_supported(conn, OSYNC_PLUGIN_CONNECTION_SERIAL)) {
		printf("\t\tSerial\n");

		if ((speed = osync_plugin_connection_serial_get_speed(conn)))
			printf("\t\t\tSpeed: %u\n", speed);

		if ((devicenode = osync_plugin_connection_serial_get_devicenode(conn)))
			printf("\t\t\tDeviceNode: %s\n", devicenode);
	}


	/* IrDA */
	if (osync_plugin_connection_is_supported(conn, OSYNC_PLUGIN_CONNECTION_IRDA)) {
		printf("\t\tIrDA\n");

		if ((service = osync_plugin_connection_irda_get_service(conn)))
			printf("\t\t\tService: %s\n", service);
	}

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_localization(OSyncPluginLocalization *local)
{
	const char *encoding, *tz, *language;

	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, local);
	
	printf("\tLocalization\n");

	if ((encoding = osync_plugin_localization_get_encoding(local)))
		printf("\t\tEncoding: %s\n", encoding);

	if ((tz = osync_plugin_localization_get_timezone(local)))
		printf("\t\tTimezone: %s\n", tz);

	if ((language = osync_plugin_localization_get_language(local)))
		printf("\t\tLanguage: %s\n", language);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_resource_format(OSyncObjFormatSink *format_sink)
{
	const char *name, *config;

	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, format_sink);

	name = osync_objformat_sink_get_objformat(format_sink);
	config = osync_objformat_sink_get_config(format_sink);

        printf("\t\t\t\t%s", name);
	if (config)
		printf(" - config: %s\n", config);
	else
		printf("\n");

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_resource(OSyncPluginResource *res)
{
	OSyncList *o = NULL, *objformats = NULL;
	const char *preferred_format, *name, *mime, *objtype, *path, *url;
	osync_bool res_enabled;

	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, res);

        objtype = osync_plugin_resource_get_objtype(res);
	res_enabled = osync_plugin_resource_is_enabled(res);
	printf("\t\tObjtype: %s (%s)\n", objtype ? objtype : "*** objtype not set ***", res_enabled ? "enabled" : "disabled");

	printf("\t\t\tFormats\n");
	objformats = osync_plugin_resource_get_objformat_sinks(res);
	for (o = objformats; o; o = o->next) {
		OSyncObjFormatSink *format_sink = o->data;
		osynctool_print_config_resource_format(format_sink);
	}
	osync_list_free(objformats);
	
	if ((preferred_format = osync_plugin_resource_get_preferred_format(res)))
		printf("\t\t\tPreferred: %s\n", preferred_format);

	if ((name = osync_plugin_resource_get_name(res)))
		printf("\t\t\tName: %s\n", name);

	if ((mime = osync_plugin_resource_get_mime(res)))
		printf("\t\t\tMIME: %s\n", mime);

	if ((path = osync_plugin_resource_get_path(res)))
		printf("\t\t\tPath: %s\n", path);

	if ((url = osync_plugin_resource_get_url(res)))
		printf("\t\t\tUrl: %s\n", url);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_resources(OSyncList *resources)
{
	OSyncList *res;

	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, resources);

	printf("\tResources\n");

	for (res = resources; res; res = res->next)
		osynctool_print_config_resource(res->data);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_advancedoption_param(OSyncPluginAdvancedOptionParameter *param)
{
	OSyncList *v = NULL;
	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, param);

	printf("\t\t\tParameter\n");

	if (osync_plugin_advancedoption_param_get_displayname(param))
		printf("\t\t\t\tDisplayName: %s\n", osync_plugin_advancedoption_param_get_displayname(param));

	if (!osync_plugin_advancedoption_param_get_name(param))
		printf("\t\t\t\tName: *** not set ***\n");
	else
		printf("\t\t\t\tName: %s\n", osync_plugin_advancedoption_param_get_name(param));

	if (!osync_plugin_advancedoption_param_get_type(param))
		printf("\t\t\t\tType: *** not set ***\n");
	else
		printf("\t\t\t\tType: %s\n", osync_plugin_advancedoption_param_get_type_string(param));

	OSyncList *valenums = osync_plugin_advancedoption_param_get_valenums(param);
	if (valenums)
		printf("\t\t\t\tEnum Values:\n");
	for (v = valenums; v; v = v->next) {
		char *valenum = v->data;
		printf("\t\t\t\t\t%s\n", valenum);
	}
	osync_list_free(valenums);

	if (!osync_plugin_advancedoption_param_get_value(param))
		printf("\t\t\t\tValue: *** not set ***\n");
	else
		printf("\t\t\t\tValue: %s\n", osync_plugin_advancedoption_param_get_value(param));

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_advancedoption(OSyncPluginAdvancedOption *option)
{
	OSyncList *p;
	OSyncList *v;
	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, option);

	printf("\t\tAdvanced Option\n");

	if (osync_plugin_advancedoption_get_displayname(option))
		printf("\t\t\tDisplayName: %s\n", osync_plugin_advancedoption_get_displayname(option));

	if (osync_plugin_advancedoption_get_maxoccurs(option))
		printf("\t\t\tMaxOccurs: %u\n", osync_plugin_advancedoption_get_maxoccurs(option));

	if (osync_plugin_advancedoption_get_max(option))
		printf("\t\t\tMax: %u\n", osync_plugin_advancedoption_get_max(option));

	if (osync_plugin_advancedoption_get_min(option))
		printf("\t\t\tMin: %u\n", osync_plugin_advancedoption_get_min(option));

	if (!osync_plugin_advancedoption_get_name(option))
		printf("\t\t\tName: *** not set ***\n");
	else
		printf("\t\t\tName: %s\n", osync_plugin_advancedoption_get_name(option));

	OSyncList *parameters = osync_plugin_advancedoption_get_parameters(option);
	for (p = parameters; p; p = p->next) {
		OSyncPluginAdvancedOptionParameter *param = p->data;
		osynctool_print_config_advancedoption_param(param);
	}
	osync_list_free(parameters);
	
	if (!osync_plugin_advancedoption_get_type(option))
		printf("\t\t\tType: *** not set ***\n");
	else
		printf("\t\t\tType: %s\n", osync_plugin_advancedoption_get_type_string(option));

	OSyncList *valenums = osync_plugin_advancedoption_get_valenums(option);
	if (valenums)
		printf("\t\t\tEnum Values:\n");
	for (v = valenums; v; v = v->next) {
		char *valenum = v->data;
		printf("\t\t\t\t%s\n", valenum);
	}
	osync_list_free(valenums);
	
	/* Value */
	if (!osync_plugin_advancedoption_get_value(option))
		printf("\t\t\tValue: *** not set ***\n");
	else
		printf("\t\t\tValue: %s\n", osync_plugin_advancedoption_get_value(option));

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

static void
osynctool_print_config_advancedoptions(OSyncList *options)
{
	OSyncList *o;

	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, options);

	printf("\tAdvanced Options\n");

	for (o = options; o; o = o->next)
		osynctool_print_config_advancedoption(o->data);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

void osynctool_print_config(OSyncPluginConfig *config)
{
	OSyncPluginConnection *conn;
	OSyncPluginAuthentication *auth;
	OSyncPluginLocalization *local;
	OSyncPluginExternalPlugin *externalplugin;
	OSyncList *resources;
	OSyncList *options;

	osync_assert(config);
	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, config);

	/* Advanced Options */
	if ((options = osync_plugin_config_get_advancedoptions(config)))
		osynctool_print_config_advancedoptions(options);

	/* Authentication */
	if ((auth = osync_plugin_config_get_authentication(config)))
		osynctool_print_config_authentication(auth);

	/* Connection */
	if ((conn = osync_plugin_config_get_connection(config)))
		osynctool_print_config_connection(conn);

	/* Localization */
	if ((local = osync_plugin_config_get_localization(config)))
		osynctool_print_config_localization(local);

	/* Resources */
	if ((resources = osync_plugin_config_get_resources(config)))
		osynctool_print_config_resources(resources);

	/* ExternalPlugin */
	if ((externalplugin = osync_plugin_config_get_externalplugin(config)))
		osynctool_print_config_externalplugin(externalplugin);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;
}

