## This file should be placed in the root directory of your project.
## Then modify the CMakeLists.txt file in the root directory of your
## project to incorporate the testing dashboard.
## # The following are required to uses Dart and the Cdash dashboard
##   ENABLE_TESTING()
##   INCLUDE(Dart)
set(CTEST_PROJECT_NAME "osynctool")
set(CTEST_NIGHTLY_START_TIME "00:00:00 CST")

set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "opensync.org")
set(CTEST_DROP_LOCATION "/testing/submit.php?project=osynctool")
set(CTEST_DROP_SITE_CDASH TRUE)
